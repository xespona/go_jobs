package main

import (
	"github.com/streadway/amqp"
	"./configReader"
	"log"
	"encoding/json"
	"os/exec"
	"os"
	"bytes"
	"strconv"
	"time"
	"sync/atomic"
	"net/smtp"
	"math/rand"
)

type ProductSearchMessage struct {
	ProductSearchId string `json:"product_search_id"`
}

func consume(i int) {
	defer finishConsumer(i)
	log.Printf("I'm a consumer! My number is %s", strconv.Itoa(i))
	connection, err := amqp.Dial("amqp://test:test@localhost:25672")
	if err != nil {
		log.Printf("Consumer %s Error connecting: %s", strconv.Itoa(i), err)
		panic("")
	}

	channel, err := connection.Channel()
	if err != nil {
		log.Printf("Consumer %s Error with channel: %s", strconv.Itoa(i), err)
		panic("")
	}

	messages, err := channel.Consume(
		"product_search",            //queue
		"go_worker"+strconv.Itoa(i), //consumer tag
		false,                       //send ack to rabbit on consumption
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Printf("Consumer %s Error consuming: %s", strconv.Itoa(i), err)
		panic("")
	}

	log.Printf("C 0 - Consumer number %s is ready to read messages", strconv.Itoa(i))

	var numberOfMessagesBeingProcessed int64 = 0
	var mailSent int64 = 0
	messagesChannel := make(chan bool, 1)

	for message := range messages {
		log.Printf("C 1 - Consumer %s read %s", strconv.Itoa(i), message.Body)

		log.Printf("C 2 - Info: Number of messages being processed: % s", strconv.FormatInt(numberOfMessagesBeingProcessed, 10))
		if numberOfMessagesBeingProcessed > 0 {
			drainchan(messagesChannel)

			log.Printf("C 3 - There are more than 100 messages being processed, i'm waiting...")
			go sendMail(numberOfMessagesBeingProcessed, &mailSent, i)
			<-messagesChannel
			log.Printf("C 4 - ACK arrived, end wait")

		} else {
			log.Printf("C 3 - I DO NOT HAVE TO WAIT! :D")
		}

		atomic.AddInt64(&numberOfMessagesBeingProcessed, +1)
		go decodeAndExecute(message, &numberOfMessagesBeingProcessed, messagesChannel)
	}

	log.Printf("Error: Ranging over messages has finished, this should never happen")
	panic("")
}

func drainchan(channel chan bool) {
	for {
		select {
		case e := <-channel:
			_ = e
		default:
			return
		}
	}
}

func decodeAndExecute(message amqp.Delivery, numberOfMessagesBeingProcessed *int64, messagesChannel chan<- bool) {
	messageBody := message.Body
	log.Printf("D 5 - Decode and execute recieved %s", messageBody)

	decodedMessage := ProductSearchMessage{}
	err := json.Unmarshal(messageBody, &decodedMessage)
	if err != nil {
		log.Printf("Error decoding message: %s", err)
	}

	execute(message, decodedMessage.ProductSearchId)

	atomic.AddInt64(numberOfMessagesBeingProcessed, -1)

	messagesChannel <- true
}

func execute(message amqp.Delivery, productSearchIdentity string) {
	log.Printf("E 6 - Execute %s", productSearchIdentity)

	//Exec
	commandPath := configReader.Path("config.json")
	environment := configReader.Environment("config.json")
	cmd := exec.Command("php", commandPath, "--env="+environment, "product_search:execute", productSearchIdentity)
	cmdOutput := &bytes.Buffer{}
	cmd.Stdout = cmdOutput

	// This random is make that 50% of commands have a timeout of 3s (to force them to fail) so we can see jobs requeued
	if rand.Int()%2 == 1 {
		time.AfterFunc(3*time.Second, func() {
			cmd.Process.Kill()
		})
	}
	err := cmd.Run()
	log.Printf("E 7 - CMD OUTPUT: " + string(cmdOutput.Bytes()))
	if err != nil {
		os.Stderr.WriteString(err.Error())
		log.Printf("E 8 - End execute with error for product search %s", productSearchIdentity)
		message.Nack(false, true)
	} else {
		log.Printf("E 8 - End execute with OK for product search %s", productSearchIdentity)
		message.Ack(false)
	}
}

func main() {
	i := 0
	for {
		log.Printf("Spawning consumer %s", strconv.Itoa(i))
		consume(i)
		log.Printf("Sleeping 2 second")
		time.Sleep(time.Second * 2)
		i = i + 1
	}
}

func finishConsumer(i int) {
	log.Printf("MUERTE - Consumer %s has ended", strconv.Itoa(i))
	if r := recover(); r != nil {
		log.Printf("Consumer %s has recovered from panic, proceeding to shut down gracefully", strconv.Itoa(i))

	}
}

func sendMail(numberOfMessagesBeingProcessed int64, mailSent *int64, consumerId int) {
	defer mailFailed(consumerId)

	if atomic.LoadInt64(mailSent) == 0 {

		atomic.AddInt64(mailSent, +1)
		time.AfterFunc(1*time.Minute, func() {
			atomic.StoreInt64(mailSent, 0)
		})

		emailTo := configReader.EmailTo("config.json")
		emailFrom := configReader.EmailFrom("config.json")

		// Set up authentication information.
		auth := smtp.PlainAuth("", "", "", "localhost")

		// Connect to the server, authenticate, set the sender and recipient,
		// and send the email all in one step.
		to := []string{emailTo}
		msg := []byte("To: " + emailTo + "\r\n" +
			"Subject:Alert Workers reaching limit\r\n" +
			"\r\n" +
			"Number of active workers" + strconv.FormatInt(numberOfMessagesBeingProcessed, 10) + ".\r\n")
		err := smtp.SendMail("localhost:25", auth, emailFrom, to, msg)

		if err != nil {
			log.Printf("Error sending mail: %s", err)
			panic("")
		}
	}
}

func mailFailed(consumerId int) {
	if r := recover(); r != nil {
		log.Printf("Consumer %s has recovered from panic after sending mail, proceeding to shut down gracefully", strconv.Itoa(consumerId))
	}
}
