package configReader

import (
	"encoding/json"
	"os"
	"fmt"
	"path/filepath"
)

type Configuration struct {
	Path string	`json:"Path"`
	Environment	string `json:"Environment"`
	EmailTo string `json:"EmailTo"`
	EmailFrom string `json:"EmailFrom"`
}

func read (filename string) Configuration {
	absPath, _ := filepath.Rel("./",filename)
	file, err := os.Open(absPath)
	if err != nil {
		fmt.Println("error:", err)
	}
	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	err = decoder.Decode(&configuration)
	if err != nil {
		fmt.Println("error:", err)
	}
	return configuration
}

func Path(filename string) string {
	configuration := read(filename)

	return configuration.Path
}

func Environment(filename string) string {
	configuration := read(filename)

	return configuration.Environment
}

func EmailTo(filename string) string {
	configuration := read(filename)

	return configuration.EmailTo
}

func EmailFrom(filename string) string {
	configuration := read(filename)

	return configuration.EmailFrom
}